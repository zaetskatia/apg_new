package com.eza.apg;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.eza.apg.ProgramList.ProgramListFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements IProgramButtonClickListener
{
    List<Integer> temButteryStore = new ArrayList<>();
    boolean isReadyForBattery;
    boolean isOnButton = true;

    Button onOffButton;
    ImageView batteryImageView;
    View signalImageView;
    LinearLayout signalLineContainer;

    MainFragment mfrag;
    ProgramFragment pfrag;

    public String recievedString;
    public String recievedStringRecover = "";
    private Bluetooth btInterface;
    EProgram currentProgram;
    boolean isRecoverConnection = false;
    FreqModel currentModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setTitle("Medbiotech");

        initElements();

        btInterface = new Bluetooth();

        if (Bluetooth.blue_ENABLE == 1)
        {
            startActivityForResult(btInterface.enableBtIntent, 1);
            if(Bluetooth.status == 10)
            {
                Toast.makeText(getBaseContext(), "...Connection is NOT ESTABLISHED - try to restart the application ", Toast.LENGTH_SHORT).show();
            }
        }
        else if (Bluetooth.blue_ENABLE == 0)
        {
            Toast.makeText(getBaseContext(), "Need to turn on Bluetooth!", Toast.LENGTH_SHORT).show();
        }
        else if (Bluetooth.blue_ENABLE == 2)
        {
            Toast.makeText(getBaseContext(), "Bluetooth devices not found", Toast.LENGTH_SHORT).show();
        }

        pfrag = new ProgramFragment();

        mfrag = new MainFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frgmCont, mfrag);
        ft.commit();

        Bluetooth.writeData((byte) 7);

        testConnection();

    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == 0)
//        {
//            Toast.makeText(getBaseContext(), "Need to turn on Bluetooth!", Toast.LENGTH_SHORT).show();
//        }
//        if (requestCode == 1)
//        {
//            btInterface = new Bluetooth();
//            if(Bluetooth.status == 10)
//            {
//                Toast.makeText(getBaseContext(), "...Connection is NOT ESTABLISHED - try to restart the application ", Toast.LENGTH_SHORT).show();
//
//            }
//        }
//    }

    private void initElements()
    {
        batteryImageView = (ImageView) findViewById(R.id.batteryImageView);
        signalImageView = findViewById(R.id.signalLineView);
        signalLineContainer = (LinearLayout) findViewById(R.id.signalLineContainer);
        onOffButton = (Button) findViewById(R.id.onOffButton);

        onOffButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Bluetooth.writeData((byte) 9);
                if (isOnButton)
                {
                    Bluetooth.writeData((byte) 0);
                    onOffButton.setText("Off");
                    isOnButton = false;
                }
                else
                {
                    Bluetooth.writeData((byte) 1);
                    onOffButton.setText("On");
                    isOnButton = true;
                }
            }
        });
    }

    private  void testConnection()
    {
        Bluetooth.hBlue = new Handler() {
            @Override
            public void handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case 1: {
                        byte[] readBuf = (byte[]) msg.obj;

                        if(readBuf.length == 4 )
                        {
                           // Toast.makeText(getBaseContext(), "recieved: " + readBuf[0]+ " "+ readBuf[1]+ " "+ readBuf[2]+ " " + readBuf[3], Toast.LENGTH_SHORT).show();
                            TimerController.getInstance().stopTimer();
                            setSignalLevel(readBuf[0] * 256 + Extentions.ToUnsigned(readBuf[1]), readBuf[2] * 256 + Extentions.ToUnsigned(readBuf[3]));
                            return;
                        }

                        recievedString = Math.abs(readBuf[0]) + "";
                        if(readBuf.length == 2)
                        {
                            recievedStringRecover = Math.abs(readBuf[1]) + "";
                        }

                        if (recievedString.equals("25"))
                        {
                            //Toast.makeText(getBaseContext(), "recieved: " + recievedString, Toast.LENGTH_SHORT).show();
                            GoToMainFragment();

                            return;
                        }

                        if (recievedString.equals("49") && !isReadyForBattery)
                        {
                           // Toast.makeText(getBaseContext(), "recieved: " + recievedString, Toast.LENGTH_SHORT).show();
                            TimerController.getInstance().startTimer();
                            isReadyForBattery = true;
                            return;
                        }


                        if (recievedString.equals("31"))
                        {
                          //  Toast.makeText(getBaseContext(), "recieved: " + recievedString, Toast.LENGTH_SHORT).show();
                            isRecoverConnection = true;
                        }

                        if (recievedString.equals("11") || recievedStringRecover.equals("11"))
                        {
                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.replace(R.id.frgmCont, pfrag);
                            ft.commit();

                          //  Toast.makeText(getBaseContext(), "recieved: " + recievedString, Toast.LENGTH_SHORT).show();
                           // pfrag.drawCircle(R.mipmap.circle);
                            return;
                        }

                        if (recievedString.equals("12")|| recievedStringRecover.equals("12"))
                        {
                           // Toast.makeText(getBaseContext(), "recieved: " + recievedString, Toast.LENGTH_SHORT).show();

                            if(isRecoverConnection)
                            {
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.replace(R.id.frgmCont, pfrag);
                                ft.commit();
                                isRecoverConnection = false;
                                return;
                            }

                            pfrag.drawCircle(R.mipmap.circle1);
                            return;
                        }
                        if (recievedString.equals("13")|| recievedStringRecover.equals("13"))
                        {
                          //  Toast.makeText(getBaseContext(), "recieved: " + recievedString, Toast.LENGTH_SHORT).show();

                            if(isRecoverConnection)
                            {
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.replace(R.id.frgmCont, pfrag);
                                ft.commit();
                                isRecoverConnection = false;
                                return;
                            }

                            pfrag.drawCircle(R.mipmap.circle2);
                            return;
                        }
                        if (recievedString.equals("14")|| recievedStringRecover.equals("14"))
                        {
                          //  Toast.makeText(getBaseContext(), "recieved: " + recievedString, Toast.LENGTH_SHORT).show();

                            if(isRecoverConnection)
                            {
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.replace(R.id.frgmCont, pfrag);
                                ft.commit();
                                isRecoverConnection = false;
                                return;
                            }

                            pfrag.drawCircle(R.mipmap.circle3);
                            return;
                        }
                        if (recievedString.equals("15")|| recievedStringRecover.equals("15"))
                        {
                          //  Toast.makeText(getBaseContext(), "recieved: " + recievedString, Toast.LENGTH_SHORT).show();

                            if(isRecoverConnection)
                            {
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.replace(R.id.frgmCont, pfrag);
                                ft.commit();
                                isRecoverConnection = false;
                                return;
                            }

                            pfrag.drawCircle(R.mipmap.circle4);
                            return;
                        }

                        else
                        {
//                            temButteryStore.add(Math.abs(readBuf[0]));
//
//                            if(temButteryStore.size() == 4)
//                            {
//                                TimerController.getInstance().stopTimer();
//                                setSignalLevel(temButteryStore.get(0) * 256 + Extentions.ToUnsigned(temButteryStore.get(1)), temButteryStore.get(2) * 256 + Extentions.ToUnsigned(temButteryStore.get(3)));
//                                temButteryStore.clear();
//                            }
                            setSignalLevel(Extentions.ToUnsigned(readBuf[0]), 0);

                            //Toast.makeText(getBaseContext(), "in else : not handle " + recievedString, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        };
    }

    void sendData(final byte[] byteArray, int progNumber)
    {
        TimerController.getInstance().stopTimer();

        Bluetooth.writeData((byte)2);

        for (int i = 0; i < byteArray.length; i++)
        {
            Bluetooth.writeData(byteArray[i]);
        }

        Bluetooth.writeData((byte)progNumber);
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Bluetooth.writeData((byte)3);
    }


    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Stop");
        alertDialog.setMessage("Are you sure to stop program?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog,int which)
            {
                customBack();

            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    void customBack()
    {
        TimerController.getInstance().stopTimer();
        Bluetooth.writeData((byte) 5);
        super.onBackPressed();
        finish();
    }

    @Override
    public void programButtonClick(EProgram program)
    {
        switch(program)
        {
            case PROGRAM1:
                sendData(FreqModelHolderTen.fraqArray1, 1);
                currentProgram = program;
                break;
            case PROGRAM2:
                sendData(FreqModelHolderTen.fraqArray2, 2);
                currentProgram = program;
                break;
            case PROGRAM3:
                sendData(FreqModelHolderTen.fraqArray3, 3);
                currentProgram = program;
                break;
            case PROGRAM4:
                sendData(FreqModelHolderTen.fraqArray4, 4);
                currentProgram = program;
                break;
            case PROGRAM5:
                sendData(FreqModelHolderTen.fraqArray5, 5);
                currentProgram = program;
                break;
            case PROGRAM6:
                sendData(FreqModelHolderTen.fraqArray6, 6);
                currentProgram = program;
                break;
            case PROGRAM7:
                sendData(FreqModelHolderTen.fraqArray7, 7);
                currentProgram = program;
                break;
            case PROGRAM8:
                sendData(FreqModelHolderTen.fraqArray8, 8);
                currentProgram = program;
                break;
            case PROGRAM9:
                sendData(FreqModelHolderTen.fraqArray9, 9);
                currentProgram = program;
                break;
            case PROGRAM10:
                sendData(FreqModelHolderTen.fraqArray10, 10);
                currentProgram = program;
                break;
        }
    }

    @Override
    public void programListItemClick(FreqModel program)
    {
        byte[] fraqArray = new byte[] {
                program.Hi1, program.Lo1,
                program.Hi2,program.Lo2,
                program.Hi3,program.Lo3};

        this.currentModel = program;
        currentProgram = null;
        sendData(fraqArray, program.progNumber);
    }

    @Override
    public void GoToMainFragment()
    {
        Fragment frag2 = new MainFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frgmCont, frag2);
        ft.commit();

        testConnection();
        TimerController.getInstance().startTimer();
    }

    @Override
    public void setProgName()
    {
        if(currentProgram == null && currentModel != null)
        {
            pfrag.setUpProgramNameText(currentModel.getName());
            return;
        }
        switch(currentProgram) {
            case PROGRAM1:
                pfrag.setUpProgramNameText(FreqModelHolderTen.freqModel1.name);
                break;
            case PROGRAM2:
                pfrag.setUpProgramNameText(FreqModelHolderTen.freqModel2.name);
                break;
            case PROGRAM3:
                pfrag.setUpProgramNameText(FreqModelHolderTen.freqModel3.name);
                break;
            case PROGRAM4:
                pfrag.setUpProgramNameText(FreqModelHolderTen.freqModel4.name);
                break;
            case PROGRAM5:
                pfrag.setUpProgramNameText(FreqModelHolderTen.freqModel5.name);
                break;
            case PROGRAM6:
                pfrag.setUpProgramNameText(FreqModelHolderTen.freqModel6.name);
                break;
            case PROGRAM7:
                pfrag.setUpProgramNameText(FreqModelHolderTen.freqModel7.name);
                break;
            case PROGRAM8:
                pfrag.setUpProgramNameText(FreqModelHolderTen.freqModel8.name);
                break;
            case PROGRAM9:
                pfrag.setUpProgramNameText(FreqModelHolderTen.freqModel9.name);
                break;
            case PROGRAM10:
                pfrag.setUpProgramNameText(FreqModelHolderTen.freqModel10.name);
                break;
        }
    }

    @Override
    public void drawableState()
    {
        isRecoverConnection = false;
        if(recievedStringRecover.equals("12"))
        {
            pfrag.drawCircle(R.mipmap.circle1);
        }
        if(recievedStringRecover.equals("13"))
        {
            pfrag.drawCircle(R.mipmap.circle2);
        }
        if(recievedStringRecover.equals("14"))
        {
            pfrag.drawCircle(R.mipmap.circle3);
        }
        if(recievedStringRecover.equals("15"))
        {
            pfrag.drawCircle(R.mipmap.circle4);
        }
        recievedStringRecover = "";
    }

    @Override
    public void goToProgramListFragment()
    {
        ProgramListFragment frag = new ProgramListFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frgmCont, frag);
        ft.commit();
    }

    private  void setSignalLevel(int butteryLevel, int signalLevel)
    {
        //---signal
        final int maxLevel = 850;
        final int minLevel = 132;

        if( signalLevel < minLevel)
        {
            signalLevel = minLevel;
        }

        if( signalLevel > maxLevel)
        {
            signalLevel = maxLevel;
        }

        final int finalSignalLevel = signalLevel;
        //---signal



        //---buttery
        final int maxButteryLevel = 236;
        final int minButteryLevel = 192;


        if (butteryLevel <= minButteryLevel)
        {
            butteryLevel = minButteryLevel;
        }

        if(butteryLevel >= minButteryLevel && butteryLevel <= minButteryLevel + 10)
        {
        }


        else if (butteryLevel > maxButteryLevel)
        {
            butteryLevel = maxButteryLevel;
        }

        int diff = maxButteryLevel - minButteryLevel;

        final int pecent25 = (diff * 25) / 100 + minButteryLevel;
        final int pecent50 = (diff * 50) / 100 + minButteryLevel;
        final int pecent75 = (diff * 75) / 100 + minButteryLevel;


        final int finalButteryLevel = butteryLevel;
        //---buttery

        signalLineContainer.post(new Runnable()
        {
            @Override
            public void run()
            {
                int maxWdth =  signalLineContainer.getWidth();
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)signalImageView.getLayoutParams();

                int diff = maxLevel - minLevel;
                int sigDif = finalSignalLevel - minLevel;

                int res = sigDif * maxWdth / diff;
                layoutParams.width = res;

                signalImageView.setLayoutParams(layoutParams);

                if (finalButteryLevel >= minButteryLevel && finalButteryLevel <= pecent25)
                {
                    batteryImageView.setBackgroundResource(R.mipmap.battery1);
                }
                else if (finalButteryLevel > pecent25 && finalButteryLevel <= pecent50)
                {
                    batteryImageView.setBackgroundResource(R.mipmap.battery2);

                }
                else if (finalButteryLevel > pecent50 && finalButteryLevel <= pecent75)
                {
                    batteryImageView.setBackgroundResource(R.mipmap.battery3);

                }
                else if (finalButteryLevel > pecent75 && finalButteryLevel <= maxButteryLevel)
                {
                    batteryImageView.setBackgroundResource(R.mipmap.battery4);

                }

                TimerController.getInstance().startTimer();
            }
        });
    }

    private void setButteryLevel(int butteryLevel, int signalLevel)
    {
        final int maxLevel = 447;
        final int minLevel = 358;

        if (butteryLevel <= minLevel)
        {
            butteryLevel = minLevel;
        }

        else if (butteryLevel > maxLevel)
        {
            butteryLevel = maxLevel;
        }

        int diff = maxLevel - minLevel;

        final int pecent25 = (diff * 25) / 100 + minLevel;
        final int pecent50 = (diff * 50) / 100 + minLevel;
        final int pecent75 = (diff * 75) / 100 + minLevel;


        final int finalButteryLevel = butteryLevel;
        batteryImageView.post(new Runnable() {
            @Override
            public void run() {

                if (finalButteryLevel >= minLevel && finalButteryLevel <= pecent25)
                {
                    batteryImageView.setBackgroundResource(R.mipmap.battery1);

                }
                else if (finalButteryLevel > pecent25 && finalButteryLevel <= pecent50)
                {
                    batteryImageView.setBackgroundResource(R.mipmap.battery2);

                }
                else if (finalButteryLevel > pecent50 && finalButteryLevel <= pecent75)
                {
                    batteryImageView.setBackgroundResource(R.mipmap.battery3);

                }
                else if (finalButteryLevel > pecent75 && finalButteryLevel <= maxLevel)
                {
                    batteryImageView.setBackgroundResource(R.mipmap.battery4);

                }
            }
        });
    }

}
