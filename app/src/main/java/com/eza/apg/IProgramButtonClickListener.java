package com.eza.apg;

/**
 * Created by eza on 18.02.18.
 */

public interface IProgramButtonClickListener {
    void programButtonClick(EProgram program);
    void programListItemClick(FreqModel program);
    void GoToMainFragment();
    void setProgName();
    void drawableState();
    void goToProgramListFragment();

}
