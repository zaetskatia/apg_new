package com.eza.apg.ProgramList;

import android.widget.Filter;

import com.eza.apg.FreqModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eza on 27.10.2018.
 */

public class CustomFilterMain extends Filter {
    ProgramListAdapter adapter;
    public List<FreqModel> illnessTitles;
    public CustomFilterMain(List<FreqModel> illnessTitles, ProgramListAdapter adapter)
    {
        this.adapter=adapter;
        this.illnessTitles=illnessTitles;
    }
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();

        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<FreqModel> filteredIllnesses = new ArrayList<>();
            for (int i = 0; i < illnessTitles.size(); i++)
            {

                if(illnessTitles.get(i).getName().toUpperCase().contains(constraint))
                {
                    filteredIllnesses.add(illnessTitles.get(i));
                }
            }
            results.count=filteredIllnesses.size();
            results.values=filteredIllnesses;
        }
        else
        {
            results.count=illnessTitles.size();
            results.values=illnessTitles;
        }
        return results;
    }
    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.categoryTitles = (ArrayList<FreqModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
