package com.eza.apg.ProgramList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.recyclerview.widget.RecyclerView;

import com.eza.apg.FreqModel;
import com.eza.apg.IProgramButtonClickListener;
import com.eza.apg.R;

import java.util.List;

/**
 * Created by eza on 27.10.2018.
 */

public class ProgramListAdapter extends RecyclerView.Adapter implements Filterable {

    public List<FreqModel> categoryTitles;
    private CustomFilterMain filter;
    private IProgramButtonClickListener programButtonClickListener;

    public ProgramListAdapter(List<FreqModel> categoryTitles, IProgramButtonClickListener programButtonClickListener)
    {
        this.categoryTitles = categoryTitles;
        this.programButtonClickListener = programButtonClickListener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.program_card, parent, false);

        MainListViewHolder vh = new MainListViewHolder(v, this);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MainListViewHolder mainListViewHolder = (MainListViewHolder) holder ;

        FreqModel item = categoryTitles.get(position);
        mainListViewHolder.setData(item);

    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomFilterMain(categoryTitles, this);
        }
        return filter;
    }

    @Override
    public int getItemCount() {
        return categoryTitles.size();
    }

    public  void goToDetailListClick(FreqModel model)
    {
        programButtonClickListener.programListItemClick(model);
    }


}
