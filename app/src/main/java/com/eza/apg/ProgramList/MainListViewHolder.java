package com.eza.apg.ProgramList;

import android.graphics.Color;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.eza.apg.FreqModel;
import com.eza.apg.R;


/**
 * Created by eza on 27.10.2018.
 */

public class MainListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private ProgramListAdapter mainListAdapter;
    TextView categoryName;
    FreqModel currentItem;
    LinearLayout cardBackground;

    public MainListViewHolder(View itemView, ProgramListAdapter mainListAdapter)
    {
        super(itemView);
        itemView.setOnClickListener(this);
        this.mainListAdapter = mainListAdapter;

        categoryName = (TextView)itemView.findViewById(R.id.programName);
        cardBackground = (LinearLayout)itemView.findViewById(R.id.cardBackground);
        cardBackground.setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    public void onClick(View v)
    {
       // int currentPosition = Integer.parseInt(currentItem.getCategoryId());
        cardBackground.setBackgroundColor(Color.LTGRAY);
        mainListAdapter.goToDetailListClick(currentItem);
    }

    public void setData(FreqModel item)
    {
        categoryName.setText(item.getName());
        currentItem = item;
    }
}
