package com.eza.apg.ProgramList;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eza.apg.FreqModel;
import com.eza.apg.FreqModelHolder;
import com.eza.apg.IProgramButtonClickListener;
import com.eza.apg.R;

import java.util.List;

/**
 * Created by eza on 27.10.2018.
 */

public class ProgramListFragment extends Fragment {

    private RecyclerView recyclerView;
    private ProgramListAdapter mainListAdapter;
    public List<FreqModel> categoryTitles;
    private SearchView searchView;
    private Button goToMainButton;
    LayoutInflater inflater;
    View view;

    IProgramButtonClickListener programButtonClickListener;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            programButtonClickListener = (IProgramButtonClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflater=inflater;
        view = this.inflater.inflate(R.layout.fragment_program_list, null);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        goToMainButton = (Button)view.findViewById(R.id.goToMainButton);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(layoutManager);
        searchView = (SearchView) view.findViewById(R.id.mainSearch);

        categoryTitles = new FreqModelHolder(this.getActivity()).GetFreqs();

        mainListAdapter = new ProgramListAdapter(categoryTitles, programButtonClickListener);
        recyclerView.setAdapter(mainListAdapter);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                mainListAdapter.getFilter().filter(query);
                return false;
            }
        });


        goToMainButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                programButtonClickListener.GoToMainFragment();
            }
        });

        return view;
    }
}
