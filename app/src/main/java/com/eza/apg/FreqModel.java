package com.eza.apg;

/**
 * Created by eza on 17.02.18.
 */

    public class FreqModel
{
    byte Hi1;
    byte Lo1;
    byte Hi2;
    byte Lo2;
    byte Hi3;
    byte Lo3;
    String name;
    int progNumber;

    public FreqModel(int Hi1, int Lo1, int Hi2, int Lo2, int Hi3, int Lo3, String name, int progNumber) {
        this.Hi1 = (byte) Hi1;
        this.Lo1 = (byte) Lo1;
        this.Hi2 = (byte) Hi2;
        this.Lo2 = (byte) Lo2;
        this.Hi3 = (byte) Hi3;
        this.Lo3 = (byte) Lo3;
        this.name = name;
        this.progNumber = progNumber;
    }

    public FreqModel(FreqModel another) {
        this.Hi1 = another.Hi1;
        this.Lo1 = another.Lo1;
        this.Hi2 = another.Hi2;
        this.Lo2 = another.Lo2;
        this.Hi3 = another.Hi3;
        this.Lo3 = another.Lo3;
        this.name = another.name;
        this.progNumber = another.progNumber;
    }


    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
