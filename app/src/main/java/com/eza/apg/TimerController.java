package com.eza.apg;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by eza on 08.03.18.
 */
public class TimerController {
    Timer timer;
    TimerTask mTimerTask;

    private static TimerController ourInstance = new TimerController();

    public static TimerController getInstance() {
        return ourInstance;
    }

    private TimerController() {
    }


    public void startTimer()
    {
        if(timer != null) {
            timer.cancel();

        }
        Bluetooth.writeData((byte)4);

        timer = new Timer();
        mTimerTask = new MyTimerTask();

        timer.schedule(mTimerTask, 10000);

    }

    public void stopTimer()
    {
        if (timer!=null)
        {
            timer.cancel();
            timer = null;
        }
    }


    private class MyTimerTask extends TimerTask
    {
        @Override
        public void run()
        {
            Bluetooth.writeData((byte)4);
        }
    }
}
