package com.eza.apg;

/**
 * Created by eza on 25.03.2019.
 */

public class FreqModelHolderTen {

    /*public static FreqModel freqModel1 = new FreqModel(7, 183, 6,73, 1, 250, "Abscess", 1);
    public static FreqModel freqModel2= new FreqModel(7, 183, 6, 52, 6, 140, "Adenovirus infection", 2);
    public static FreqModel freqModel3= new FreqModel(5, 252, 6, 140, 5, 47, "Azoturia", 3);

    public static FreqModel freqModel4= new FreqModel(7, 183, 6, 140, 1, 250, "Anemia", 4);
    public static FreqModel freqModel5= new FreqModel(6, 52, 6, 140, 7, 183, "Ankylosis of the joints", 5);

    public static FreqModel freqModel6= new FreqModel(6, 140, 104, 228, 7, 183, "Anestrus", 6);
    public static FreqModel freqModel7= new FreqModel(11, 230, 7, 52, 4, 69, "Arthritis", 7);
    public static FreqModel freqModel8= new FreqModel(6, 87, 6, 140, 1, 250, "Lateral frog", 8);
    public static FreqModel freqModel9= new FreqModel(6, 52, 7, 183, 6, 92, "Lyme disease", 9);
    public static FreqModel freqModel10= new FreqModel(2, 64, 15, 170, 7, 176, "Teaser's Disease", 10);*/

    public static FreqModel freqModel1 = new FreqModel(7, 183, 6,73, 1, 250, "Пост вирус", 1);
    public static FreqModel freqModel2= new FreqModel(7, 183, 6, 52, 6, 140, "Вирусы", 2);
    public static FreqModel freqModel3= new FreqModel(5, 252, 6, 140, 5, 47, "Бактерии", 3);

    public static FreqModel freqModel4= new FreqModel(7, 183, 6, 140, 1, 250, "Fungus", 4);
    public static FreqModel freqModel5= new FreqModel(6, 52, 6, 140, 7, 183, "Паразиты", 5);

    public static FreqModel freqModel6= new FreqModel(6, 140, 104, 228, 7, 183, "Protozoa", 6);
    public static FreqModel freqModel7= new FreqModel(11, 230, 7, 52, 4, 69, "Sporozoa", 7);
    public static FreqModel freqModel8= new FreqModel(6, 87, 6, 140, 1, 250, "Нематоды", 8);
    public static FreqModel freqModel9= new FreqModel(6, 52, 7, 183, 6, 92, "Трематоды", 9);
    public static FreqModel freqModel10= new FreqModel(2, 64, 15, 170, 7, 176, "Цестоды", 10);

    public static byte[] fraqArray1 = new byte[] {freqModel1.Hi1, freqModel1.Lo1,
            freqModel1.Hi2,freqModel1.Lo2,
            freqModel1.Hi3,freqModel1.Lo3};

    public static byte[] fraqArray2 = new byte[] {freqModel2.Hi1, freqModel2.Lo1,
            freqModel2.Hi2,freqModel2.Lo2,
            freqModel2.Hi3,freqModel2.Lo3};

    public static byte[] fraqArray3 = new byte[] {freqModel3.Hi1, freqModel3.Lo1,
            freqModel3.Hi2,freqModel3.Lo2,
            freqModel3.Hi3,freqModel3.Lo3};

    public static byte[] fraqArray4 = new byte[] {freqModel4.Hi1, freqModel4.Lo1,
            freqModel4.Hi2,freqModel4.Lo2,
            freqModel4.Hi3,freqModel4.Lo3};


    public static byte[] fraqArray5 = new byte[] {freqModel5.Hi1, freqModel5.Lo1,
            freqModel5.Hi2,freqModel5.Lo2,
            freqModel5.Hi3,freqModel5.Lo3};


    public static byte[] fraqArray6 = new byte[] {freqModel6.Hi1, freqModel6.Lo1,
            freqModel6.Hi2,freqModel6.Lo2,
            freqModel6.Hi3,freqModel6.Lo3};


    public static byte[] fraqArray7 = new byte[] {freqModel7.Hi1, freqModel7.Lo1,
            freqModel7.Hi2,freqModel7.Lo2,
            freqModel7.Hi3,freqModel7.Lo3};


    public static byte[] fraqArray8 = new byte[] {freqModel8.Hi1, freqModel8.Lo1,
            freqModel8.Hi2,freqModel8.Lo2,
            freqModel8.Hi3,freqModel8.Lo3};


    public static byte[] fraqArray9 = new byte[] {freqModel9.Hi1, freqModel9.Lo1,
            freqModel9.Hi2,freqModel9.Lo2,
            freqModel9.Hi3,freqModel9.Lo3};

    public static byte[] fraqArray10 = new byte[] {freqModel10.Hi1, freqModel10.Lo1,
            freqModel10.Hi2,freqModel10.Lo2,
            freqModel10.Hi3,freqModel10.Lo3};
}

