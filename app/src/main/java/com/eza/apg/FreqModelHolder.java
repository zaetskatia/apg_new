package com.eza.apg;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eza on 18.02.18.
 */


  /*  public static List<FreqModel> initFreqModelList()
    {
        List<FreqModel> initList = new ArrayList<FreqModel>();

        for (int i = 0; i < 18; i++)
        {
            initList.add(new FreqModel(freqModel1));
            initList.add(new FreqModel(freqModel2));
            initList.add(new FreqModel(freqModel3));
            initList.add(new FreqModel(freqModel4));
            initList.add(new FreqModel(freqModel5));
            initList.add(new FreqModel(freqModel6));
            initList.add(new FreqModel(freqModel7));
            initList.add(new FreqModel(freqModel8));
            initList.add(new FreqModel(freqModel9));
            initList.add(new FreqModel(freqModel10));
        }

        for (int i = 0; i < initList.size(); i++)
        {
            int newIndex = i + 1;
            initList.get(i).progNumber = newIndex;
        }

        initList.get(10).setName("Swamp fever");
        initList.get(11).setName("Warts");
        initList.get(12).setName("Botulism");
        initList.get(13).setName("Bronchitis");
        initList.get(14).setName("Brucellosis");
        initList.get(15).setName("Bursitis of the hock oryllocus joint");
        initList.get(16).setName("Vesicular stomatitis");
        initList.get(17).setName("Venereal diseases");
        initList.get(18).setName("Venezuelan encephalomyelitis");
        initList.get(19).setName("Horses herpes virus");
        initList.get(20).setName("Horse viral arteritis");
        initList.get(21).setName("Viral encephalomyelitis of horses");
        initList.get(22).setName("Pedal Inflammation");
        initList.get(23).setName("Inflammation of the outer ear");
        initList.get(24).setName("Umbilical cord inflammation");
        initList.get(25).setName("Eastern encephalomyelitis");
        initList.get(26).setName("Dislocation of the knee cup");
        initList.get(27).setName("Gabronematosis");
        initList.get(28).setName("Hematoma");
        initList.get(29).setName("Hemiplegia of the larynx");
        initList.get(30).setName("Hemolytic anemia foals");
        initList.get(31).setName("Hepatitis");
        initList.get(32).setName("Hyperlipidemia");
        initList.get(33).setName("Hypoplasiacerebellum");
        initList.get(34).setName("Hypothyroidism");
        initList.get(35).setName("Purulent hooves inflammation");
        initList.get(36).setName("Flu horses");
        initList.get(37).setName("Hernia — inguinal hernia");
        initList.get(38).setName("Degenerative joint disease (DES)");
        initList.get(39).setName("Dehydration");
        initList.get(40).setName("Dermatophytosis");
        initList.get(41).setName("Limb deformity in foals");
        initList.get(42).setName("- ankylosis of the joints");
        initList.get(43).setName("- flexion deformation");
        initList.get(44).setName("- congenital flexion deformation");
        initList.get(45).setName("- acquired flexion deformation");
        initList.get(46).setName("- re-bending in the heads");
        initList.get(47).setName("Diarrhea");
        initList.get(48).setName("parasitic diseases");
        initList.get(49).setName("neoplasms");
        initList.get(50).setName("syndrome of insufficient absorption (malabsorption)");
        initList.get(51).setName("Dystocia (pathological labor)");
        initList.get(52).setName("Diestrus");
        initList.get(53).setName("Rain burn");
        initList.get(54).setName("Shiver");
        initList.get(55).setName("Zhabka");
        initList.get(56).setName("Stallion, possible injuries");
        initList.get(57).setName("scrotal infection");
        initList.get(58).setName("penis injuries");
        initList.get(59).setName("penile paralysis");
        initList.get(60).setName("Airborne bag disease");
        initList.get(61).setName("infection of air sacs");
        initList.get(62).setName("air bag tympania");
        initList.get(63).setName("Shuttle bone disease");
        initList.get(64).setName("Delayed placenta");
        initList.get(65).setName("Western encephalomyelitis");
        initList.get(66).setName("Constipation");
        initList.get(67).setName("Back of head");
        initList.get(68).setName("Prolonged diestrus");
        initList.get(69).setName("Malignant Edema");
        initList.get(70).setName("Infectious anemia of horses (INAL)");
        initList.get(71).setName("Cataract");
        initList.get(72).setName("Keratitis");
        initList.get(73).setName("Cysts");
        initList.get(74).setName("Uterine cysts");
        initList.get(75).setName("Intestinal Clostridiosis");
        initList.get(76).setName("Coital exanthema");
        initList.get(77).setName("Color");
        initList.get(78).setName("spastic colic");
        initList.get(79).setName("obstructive colic");
        initList.get(80).setName("Colitis-x");
        initList.get(81).setName("Stab wounds");
        initList.get(82).setName("Combined Immunodeficiency (KID)");
        initList.get(83).setName("Contagious metritis horses (CML)");
        initList.get(84).setName("Tendon contracture");
        initList.get(85).setName("Conjunctivitis");
        initList.get(86).setName("Crown");
        initList.get(87).setName("Hives");
        initList.get(88).setName("Lactation anestrus");
        initList.get(89).setName("Lactation tetany");
        initList.get(90).setName("Laminitis");
        initList.get(91).setName("Pulmonary bleeding caused by exercise");
        initList.get(92).setName("Leptospirosis");
        initList.get(93).setName("Lymphangitis of the limbs");
        initList.get(94).setName("Mastitis");
        initList.get(95).setName("Melanoma");
        initList.get(96).setName("Myoglobinuria");
        initList.get(97).setName("Horse Morbiillivirus Infection");
        initList.get(98).setName("Pumps from cinch");
        initList.get(99).setName("Pouring");
        initList.get(100).setName("Namina soles");


        initList.get(101).setName("Nanos");
        initList.get(102).setName("Nephritis");
        initList.get(103).setName("Nose bleed");
        initList.get(104).setName("Nettle burn");
        initList.get(105).setName("Onchocerciasis");
        initList.get(106).setName("Tumors");
        initList.get(107).setName("Orhit");
        initList.get(108).setName("Swelling of limbs");
        initList.get(109).setName("Swelling of the joints");
        initList.get(110).setName("Poisoning");
        initList.get(111).setName("poisonous plants");
        initList.get(112).setName("poisonous chemicals");
        initList.get(113).setName("snake bites");
        initList.get(114).setName("Horn Shoe Peeling");
        initList.get(115).setName("Parasitic diseases");
        initList.get(116).setName("Foot bone fracture");
        initList.get(117).setName("Fractures");
        initList.get(118).setName("Periodontitis");
        initList.get(119).setName("Peritonitis");
        initList.get(120).setName("Piroplasmosis");
        initList.get(121).setName("Pleurisy");
        initList.get(122).setName("Pneumonia");
        initList.get(123).setName("Polypus nasal cavity");
        initList.get(124).setName("Shaking head");
        initList.get(125).setName("Bite, air ingestion");
        initList.get(126).setName("Protozoal Horse Myeloencephalitis (PML)");
        initList.get(127).setName("Swinging");
        initList.get(128).setName("Recurrent uveitis");
        initList.get(129).setName("Rhinovirus infection");
        initList.get(130).setName("Rhodococcal pneumonia foals");
        initList.get(131).setName("Rotovirus infection");
        initList.get(132).setName("Salmonellosis");
        initList.get(133).setName("Sarcoid");
        initList.get(134).setName("Glanders");
        initList.get(135).setName("Seborrhea");
        initList.get(136).setName("Septicemia");
        initList.get(137).setName("Septic arthritis");
        initList.get(138).setName("Sesamoiditis");
        initList.get(139).setName("Heart and heart disease - rhythm disturbances - atrial fibrillation — cardiac murmur — myocarditis");
        initList.get(140).setName("Anthrax");
        initList.get(141).setName("Syndrome of insufficient absorption (malabsorption)");
        initList.get(142).setName("Sinusitis");
        initList.get(143).setName("Incidental disease of horses");
        initList.get(144).setName("Sunburn");
        initList.get(145).setName("Splint (Nakostnik)");
        initList.get(146).setName("Spondylitis");
        initList.get(147).setName("Tetanus");
        initList.get(148).setName("Joint foals");
        initList.get(149).setName("Heatstroke");
        initList.get(150).setName("Horse toxoplasmosis");
        initList.get(151).setName("Face hoof");
        initList.get(152).setName("Trauma of the saddle");
        initList.get(153).setName("Back injuries");
        initList.get(154).setName("Tendon injuries");
        initList.get(155).setName("Pelvic injuries");
        initList.get(156).setName("Skull trauma");
        initList.get(157).setName("Herbal disease");
        initList.get(158).setName("Cracks in the heel area of ​​the hoof");
        initList.get(159).setName("Hoofed horn cracks");
        initList.get(160).setName("Bruise soles");
        initList.get(161).setName("Horses filariasis");
        initList.get(162).setName("Folliculitis");
        initList.get(163).setName("Photosensitization");
        initList.get(164).setName("Furuncle");
        initList.get(165).setName("Furunculosis");
        initList.get(166).setName("Chronic itching");
        initList.get(167).setName("Chronic Obstructive Pulmonary Disease");
        initList.get(168).setName("Cystitis");
        initList.get(169).setName("Spar (soft and bone)");
        initList.get(170).setName("Encephalomyelitis");
        initList.get(171).setName("Endometritis");
        initList.get(172).setName("Endotoxemia");
        initList.get(173).setName("Epilepsy");
        initList.get(174).setName("Epiphysitis");
        initList.get(175).setName("Erlihioz horses");
        initList.get(176).setName("Ethmoidal hematoma");
        initList.get(177).setName("Peptic ulcer");
        initList.get(178).setName("Corneal ulcers");
        initList.get(179).setName("Japanese encephalomyelitis");

        return initList;
    } */

    public class FreqModelHolder {
    private static final String  DB_NAME = "fgeq";
    private static final String TABLE_NAME = "freq";
    private static final String FRIEND_NAME = "name";
    private static final String ID = "_id";
    private static final String HZ_HI1 = "fHZ_Hi";
    private static final String HZ_LO1 = "fHZ_Lo";
    private static final String HZ_HI2 = "sHZ_Hi";
    private static final String HZ_LO2 = "sHZ_Lo";
    private static final String HZ_HI3 = "tHZ_Hi";
    private static final String HZ_LO3 = "tHZ_Lo";
    private static SQLiteDatabase database;
    private Context context;

    private static List<FreqModel> freqsList = new ArrayList<>();

    public FreqModelHolder(Context context)
    {
        this.context = context;
    }

    public List<FreqModel> InitAndOpenDb()
    {
        ExternalDbOpenHelper dbOpenHelper = new ExternalDbOpenHelper(context, DB_NAME);
        database = dbOpenHelper.openDataBase();
        return fillFreinds();
    }

    public List<FreqModel> GetFreqs()
    {
        if(freqsList.isEmpty())
        {
            return InitAndOpenDb();
        }
        else
        {
            return freqsList;
        }
    }

    private List<FreqModel> fillFreinds()
    {
        Cursor friendCursor = database.query(TABLE_NAME, new String[] {FRIEND_NAME, ID, HZ_HI1, HZ_LO1, HZ_HI2, HZ_LO2, HZ_HI3, HZ_LO3 },
                null, null, null, null, null);
        friendCursor.moveToFirst();

        if (!friendCursor.isAfterLast()) {
            do
            {
                freqsList.add(new FreqModel(
                        friendCursor.getInt(2),
                        friendCursor.getInt(3),
                        friendCursor.getInt(4),
                        friendCursor.getInt(5),
                        friendCursor.getInt(6),
                        friendCursor.getInt(7),
                        friendCursor.getString(0),
                        friendCursor.getInt(1)));
            }
            while (friendCursor.moveToNext());
        }
        friendCursor.close();

        return freqsList;
    }

}
